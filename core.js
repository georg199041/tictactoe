module.exports = function(io) {
	var xo = 'x'; 
	var o = false;
	var m_players = [];
	var i = 0; 
	var size, y, x, rows;
	var steps = 0;
	var sq = x * y;

	io.sockets.on('connection', function(socket) {

		socket.on('throw_size', function(data) {
			size = y = x = rows = data;
			function makeGrid(size) {
				var grid = [];
				for (var x = 0; x < size; x+=1) {
					var gridLevel1 = [];
					for (var y = 0;y < size; y+=1) {
						gridLevel1.push('');
					}
					grid.push(gridLevel1);
				}
				return grid;
			}
			var grid = makeGrid(size);
			socket.on('process_move', function(coords, player) {
				console.log(coords, player)
				coords = coords.split('-');
				if (player.id == socket.id) {
					grid[coords[0]][coords[1]] = player.mark;
					steps++;
				}
				io.sockets.emit('mark', coords.join('-'));
				function clearGrid() {
					for (var i=0; i < grid.length - 1; i++) {
						for (var s=0; s < grid.length - 1; s++) {
							grid[i][s] = '';
							console.log(grid[i][s])
						}
					}
				};
				if(finished()) {
					clearGrid()
					io.sockets.emit('gameover', xo);
				}
			});

			// check win
			function finished() {
				var isFinished;
				function checkWinner() {
					var zers;
					var xses;
					for(j=0;j<size;j++) {
						zers=xses=0;
						for(i=0;i<size;i++) {
							if(grid[j][i]=='x') {
								zers++;
							} else if(grid[j][i]=='o') {
								xses++;
							} else {
								zers=0;
								xses=0;
							}
							if(zers>=rows) {
								isFinished = true;
							} else if(xses>=rows) {
								isFinished = true;
							}
						}
					}

					for(i=0;i<x;i++) {
						zers=xses=0;
						for(j=0;j<y;j++) {
							if(grid[j][i]=='x') {
								zers++;
								xses=0;
							} else if(grid[j][i]=='o') {
								xses++;
								zers=0;
							} else {
								zers=0;
								xses=0;
							}
							if(zers>=rows) {
								isFinished = true;
							} else if(xses>=rows) {
								isFinished = true;
							}
						}
					}


					var ii,jj;
					for(i=0;i<x;i++) {
						for(j=0;j<y;j++) {
							zers=xses=0;
							for(jj=j,ii=i;jj<y && ii>=0;jj++,ii--) {
								if(grid[jj][ii]=='x') {
									zers++;
									xses=0;
								} else if(grid[jj][ii]=='o') {
									xses++;
									zers=0;
								} else {
									zers=0;
									xses=0;
								}
								if(zers>=rows) {
									isFinished = true;
								} else if(xses>=rows) {
									isFinished = true;
								}
							}
						}
					}

					for(i=0;i<x;i++) {
						for(j=0;j<y;j++) {
							zers=xses=0;
							for(jj=j,ii=i;jj<y && ii<x;jj++,ii++) {
								if(grid[jj][ii]=='x') {
									zers++;
									xses=0;
								} else if(grid[jj][ii]=='o') {
									xses++;
									zers=0;
								} else {
									zers=0;
									xses=0;
								}
								if(zers>=rows) {
									isFinished = true;
								} else if(xses>=rows) {
									isFinished = true;
								}
							}
						}
					}

					if(steps>=sq) {
						//Ничья
						isFinished = true;
					}
				}
				checkWinner();

				return 	isFinished;		

			};
		});	
			
		socket.on('client_connected', function(player) {
			player.id = socket.id;
			player.mark = xo;
			if(xo == 'x' && o == false) {
			  xo = 'o';
			  o = true;
			} else {
			  xo = 'spectator';
			}
			m_players[i] = player;
			i++;
			socket.emit('connect_1', player, size);
			io.sockets.emit('load',m_players);
		});

		socket.on('disconnect', function() {
			var j = 0;
			var n = 0;
			var tmp = [];
		
			while ( n < m_players.length ) {
				if ( m_players[j].id == socket.id ) {
					if( m_players[j].mark == 'o' ) {
						xo = 'o';
					}
					if( m_players[j].mark == 'x' ) {
						xo = 'x';
					}
					n++;
				}
				if ( n < m_players.length ) {
					tmp[j] = m_players[n];
					j++;
					n++;
				}
			}
			if (io.sockets.clients().length <= 1) {
				clearGrid();
				xo = 'x';
				o = false;
			}
			m_players = tmp;
			i = j;
			io.sockets.emit('load', m_players);
		});
	});
}