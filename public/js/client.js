$(document).ready(function() {

	var socket = io.connect(document.location.href);
	var playerName = localStorage.getItem('PlayerName');
	var size;
	var player = {
		id: 0,
		name: playerName || prompt("Введите Ваше имя:"),
		mark: 'x'
	};

	function updatePlayer() {
		localStorage.setItem('PlayerName', player.name);
	}
	if (!playerName) {
		updatePlayer();
	}
	var xo = 'x';
	var gameover = false;
	var startGame = false;

	if ( player.name == '' ) {
		player.name = 'Аноним';
	}

	socket.on('connect', function () {
		socket.emit('client_connected', player);
	});		


	function mark(cords) {
		if(startGame) {
			if(player.mark != xo) {
				$("#stats").html('Сейчас не Ваш ход').hide().fadeIn(1000).fadeOut(1000);
			} else {
				if($('#' + cords + ' div').html() == '') {
					socket.emit("process_move", cords, player);
				} else {
					$("#stats").html('Так нельзя походить').hide().fadeIn(1000).fadeOut(1000);
				}
			}
		} else {
			$("#stats").html('Ждем второго игрока...').hide().fadeIn(1000).fadeOut(1000);
		}
	}

	socket.on('mark', function(sq) {
		sq = '#' + sq + ' div';
		if($(sq).html() == '') {
			if( xo == 'x') {
				$(sq).html('X');
				xo = 'o';
				$(sq).css('color','red');
			} else {
				$(sq).html('O');
				$(sq).css('color','blue');
				xo = 'x';
			}  
		}
	});

	socket.on('load', function(data) {
		var x = 0;
		var showedX, showedO = false;
		while( x < data.length ) {
			if(data[x].mark == 'x') {
				$("#p1").html(data[x].name + ": <span style='color:red'>X</span>");
				showedX = true;
			} else if(data[x].mark == 'o') {
				$("#p2").html(data[x].name + ": <span style='color:blue'>O</span>");
				showedO = true;
			}
			x++;
		}

		if(!showedX) $("#p1").html("Ждем...");
		if(!showedO) $("#p2").html("Ждем...");

		if(showedX && showedO) startGame = true;

	});

	socket.on('connect_1', function(data, gridSize){
		player.id = data.id;
		player.name = data.name;
		player.mark = data.mark;
		if ( player.mark == 'x' ) {
			size = prompt('Введите размер поля (напр. 3):')
		} else {
			size = gridSize;
		}
		function makeField(size) {
			var i,j;
			var txt='';

			for(j=0;j<size;j++) {
				txt+='<tr>';
				for(i=0;i<size;i++) {
					txt+="<td id='"+i+"-"+j+"'><div></div></td>";
				}
				txt+='</tr>';
			}

			$('#table').html(txt);
			$('#table td').each(function() {
				$(this).on('click', function() {
					mark($(this).attr('id'));
				});
			});
		}

		makeField(size);
		socket.emit("throw_size", size);
	});	
	socket.on('gameover', function(data){
		gameover = true;
		$("#stats").html("Конец игры!").hide().fadeIn(1000);
	});

});